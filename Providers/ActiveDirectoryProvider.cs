﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;

namespace AuthorizationServer.API.Providers
{
    public  static class DomainUserLoginProvider
    {
        public static bool ValidateCredentials(string userName, string password)
        {
            using (var pc = new PrincipalContext(ContextType.Domain, "cnx"))
            {
                bool isValid = pc.ValidateCredentials(userName, password);
                return isValid;
            }
        }
    }
}